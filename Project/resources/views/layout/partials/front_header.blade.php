	<!-- Logo Nav START -->
	<nav class="navbar navbar-expand-lg">
		<div class="container">
			<!-- Logo START -->
			<a class="navbar-brand" href="#">
				<img class="navbar-brand-item light-mode-item" src="{{URL::to('assets/images/logo.svg')}}" alt="logo">
				<img class="navbar-brand-item dark-mode-item" src="{{URL::to('assets/images/logo-light.svg')}}" alt="logo">
			</a>
			<!-- Logo END -->

			<!-- Responsive navbar toggler -->
			<button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="text-body h6 d-none d-sm-inline-block">Menu</span>
			  <span class="navbar-toggler-icon"></span>
		  </button>

			<!-- Main navbar START -->
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<!-- Nav Search START -->
				<div class="nav mt-3 mt-lg-0 px-4 flex-nowrap align-items-center">
					<div class="nav-item w-100">
						<form class="rounded position-relative">
							<input class="form-control pe-5 bg-light" type="search" placeholder="Search the blog" aria-label="Search">
							<button class="btn bg-transparent px-2 py-0 position-absolute top-50 end-0 translate-middle-y" type="submit"><i class="bi bi-search fs-5"> </i></button>
						</form>
					</div>
				</div>
				<!-- Nav Search END -->
				<ul class="navbar-nav navbar-nav-scroll ms-auto">

                    <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle active show" href="#" id="homeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">ទំព័រដើម</a>
						<ul class="dropdown-menu show" aria-labelledby="homeMenu" data-bs-popper="none">
							<li> <a class="dropdown-item {{request()->is('health')? 'active':''}}" href="{{route('front.categories')}}">សុខភាព</a></li>
							<li> <a class="dropdown-item" href="#">កម្សាន្ត</a></li>
							<li> <a class="dropdown-item" href="#">កីឡា</a></li>
							<li> <a class="dropdown-item" href="#">ទេសចរណ៍</a></li>
							<li> <a class="dropdown-item {{request()->is('category')? 'active':''}}" href="{{route('front.categories')}}">ជំនួញ</a></li>
						</ul>
					</li>

  					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle show" href="#" id="homeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">ចំណេះដឹង</a>
						<ul class="dropdown-menu show" aria-labelledby="homeMenu" data-bs-popper="none">
							<li> <a class="dropdown-item" href="#">ចំណេះដឹងទូទៅ</a></li>
							<li> <a class="dropdown-item" href="#">ប្រវត្តិសាស្ត្រ</a></li>
							<li> <a class="dropdown-item" href="#">គណិតសាស្ត្រ</a></li>
							<li> <a class="dropdown-item" href="#">អក្សរសាស្ត្រ</a></li>
							<li> <a class="dropdown-item active" href="#">អង់គ្លេស</a></li>
						</ul>
					</li>

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle show" href="#" id="homeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">បច្ចេកវិទ្យា</a>
						<ul class="dropdown-menu show" aria-labelledby="homeMenu" data-bs-popper="none">
							<li> <a class="dropdown-item" href="#">វិទ្យាសាស្ត្រពិភពលោក</a></li>
							<li> <a class="dropdown-item" href="#">ភាពជឿនលឿន</a></li>
							<li> <a class="dropdown-item" href="#">បច្ចេលវិទ្យាAI</a></li>
							<li> <a class="dropdown-item" href="#">ក្រុមNasa</a></li>
							<li> <a class="dropdown-item active" href="#">មហាអំណាចបច្ចេកវិទ្យា</a></li>
						</ul>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle show" href="#" id="homeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">កីឡា</a>
						<ul class="dropdown-menu show" aria-labelledby="homeMenu" data-bs-popper="none">
							<li> <a class="dropdown-item" href="#">បាល់ទាត់</a></li>
							<li> <a class="dropdown-item" href="#">បាល់បោះ</a></li>
							<li> <a class="dropdown-item" href="#">បាល់ទះ</a></li>
							<li> <a class="dropdown-item" href="#">កាយវប្បកម្ម</a></li>
							<li> <a class="dropdown-item active" href="#">ផ្សេងទៀត</a></li>
						</ul>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle show" href="#" id="homeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">ជីវិត​និងសង្គម</a>
						<ul class="dropdown-menu show" aria-labelledby="homeMenu" data-bs-popper="none">
							<li> <a class="dropdown-item" href="#">គំនិតមនុស្សសម័យថ្មី</a></li>
							<li> <a class="dropdown-item" href="#">នយោបាយ</a></li>
							<li> <a class="dropdown-item" href="#">ភាពជាអ្នកដឹកនាំ</a></li>
							<li> <a class="dropdown-item" href="#">សេចក្តីសុខក្នុងលោក</a></li>
							<li> <a class="dropdown-item active" href="#">សាសនាសកល</a></li>
						</ul>
					</li>


					<li class="nav-item">	<a class="nav-link" href="#">វប្បធម៌និងសិល្បៈ</a></li>

					<li class="nav-item">	<a class="nav-link" href="#">កម្សាន្ត</a></li>
				</ul>
			</div>
			<!-- Main navbar END -->
			{{-- <!-- Dark mode switch -->
			<div class="modeswitch ms-4" id="darkModeSwitch">
				<div class="switch"></div>
			</div>
		</div> --}}
	</nav>
	<!-- Logo Nav END -->


<section class="pt-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
                @for ($i = 0; $i <5; $i++)
				<!-- Card item START -->
				<div class="card border rounded-3 up-hover p-4 mb-4">
					<div class="row g-3">
						<div class="col-lg-5">
							<!-- Categories -->
							<a href="#" class="badge bg-danger mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Marketing</a>
							<a href="#" class="badge bg-dark mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Startups</a>
							<!-- Title -->
							<h2 class="card-title">
								<a href="post-single-6.html" class="btn-link text-reset stretched-link">7 common mistakes everyone makes while traveling</a>
							</h2>
							<!-- Author info -->
							<div class="d-flex align-items-center position-relative mt-3">
								<div class="avatar me-2">
									<img class="avatar-img rounded-circle" src="assets/images/avatar/0{{$i+2}}.jpg" alt="avatar">
								</div>
								<div>
									<h5 class="mb-1"><a href="#" class="stretched-link text-reset btn-link">Lori Ferguson</a></h5>
									<ul class="nav align-items-center small">
										<li class="nav-item me-3">Mar 07, 2021</li>
										<li class="nav-item"><i class="far fa-clock me-1"></i>5 min read</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- Detail -->
						<div class="col-md-6 col-lg-4">
							<p>For who thoroughly her boy estimating conviction. Removed demands expense account in outward tedious do. Particular way thoroughly unaffected projection favorable Mrs can be projecting own. Thirty it matter enable become admire in giving. See resolved goodness felicity shy civility domestic had but. Drawings offended yet answered Jennings perceive laughing six did far. </p>
						</div>
						<!-- Image -->
						<div class="col-md-6 col-lg-3">
							<img class="rounded-3" src="assets/images/blog/4by3/0{{$i+2}}.jpg" alt="Card image">
						</div>
					</div>
				</div>
				<!-- Card item END -->
                @endfor
				<!-- Load more -->
				<button type="button" class="btn btn-primary-soft w-100">Load more post <i class="bi bi-arrow-down-circle ms-2 align-middle"></i></button>

			</div>
		</div>
	</div>
</section>

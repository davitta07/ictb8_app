<section class="pt-4 pb-0">
	<div class="container">
		<div class="row">
             @for ($i = 7; $i <=14; $i++)
             <!-- Card item START -->
			<div class="col-sm-6 col-lg-3">
				<div class="card mb-4">
					<!-- Card img -->
					<div class="card-fold position-relative">
                @if ($i<=9)
						<img class="card-img" src="assets/images/blog/4by3/0{{$i}}.jpg" alt="Card image">
                 @else
						<img class="card-img" src="assets/images/blog/4by3/{{$i}}.jpg" alt="Card image">
                @endif
					</div>
					<div class="card-body px-0 pt-3">
						<h4 class="card-title"><a href="post-single.html" class="btn-link text-reset stretched-link fw-bold">7 common mistakes everyone makes while traveling</a></h4>
						<!-- Card info -->
						<ul class="nav nav-divider align-items-center text-uppercase small">
							<li class="nav-item">
								<a href="#" class="nav-link text-reset btn-link">Louis Ferguson</a>
							</li>
							<li class="nav-item">Mar 07, 2021</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- Card item END -->
            @endfor
		</div>
	</div>
</section>

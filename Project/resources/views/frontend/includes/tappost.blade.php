<section class="pt-4 pb-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs">
					<li class="nav-item"> <a class="nav-link fs-5 active" data-bs-toggle="tab" href="#tab-1-1"> <i class="fab fa-readme me-2"></i> Most read </a> </li>
					<li class="nav-item"> <a class="nav-link fs-5" data-bs-toggle="tab" href="#tab-1-2"> <i class="fas fa-fire me-2"></i> Trending </a> </li>
				</ul>
				<div class="tab-content">
					<!-- Most read tab START -->
					<div class="tab-pane show active" id="tab-1-1">
						<div class="row">
                            @for ($i = 1; $i <=20; $i++)
							<!-- Tab items group START -->
							<div class="col-md-4">
								<!-- Item -->
								<div class="d-flex position-relative mb-3">
									<span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">{{$i}}</span>
									<h5><a href="#" class="stretched-link text-reset btn-link">The most common business debate isn't as black and white as you might think</a></h5>
								</div>
							</div>
							<!-- Tab items group END -->
                             @endfor
						</div>
					</div>
					<!-- Most read tab END -->
					<!-- Trending tab START -->
					<div class="tab-pane show" id="tab-1-2">

						<div class="row">
                            @for ($i = 1; $i <=21; $i++)
							<!-- Tab items group START -->
							<div class="col-md-4">
								<!-- Item -->
								<div class="d-flex position-relative mb-3">
									<span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">{{$i}}</span>
									<h5><a href="#" class="stretched-link text-reset btn-link">The most common business debate isn't as black and white as you might think</a></h5>
								</div>
							</div>
							<!-- Tab items group END -->
                             @endfor

						</div>
					</div>

					<!-- Trending tab END -->
				</div>
			</div>
		</div>
	</div>
</section>

@extends('layout.front')
@section('main_content')
<!-- =======================
Inner intro START -->
<section class="pb-4 pt-3">
	<div class="container">
		<div class="row">
      <div class="col-12">
        <div class="card bg-dark-overlay-3 h-300 overflow-hidden card-bg-scale text-center" style="background-image:url(assets/images/blog/16by9/07.jpg); background-position: center left; background-size: cover;">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex align-items-center p-3 p-sm-4">
            <div class="col-md-8 m-auto bg-blur p-5 rounded-3 shadow-lg">
							<h1 class="text-white">Post cards</h1>
							<nav class="d-flex justify-content-center" aria-label="breadcrumb">
						<ol class="breadcrumb breadcrumb-dark breadcrumb-dots mb-0">
							<li class="breadcrumb-item"><a href="index.html"><i class="bi bi-house me-1"></i> Home</a></li>
							<li class="breadcrumb-item active">All post</li>
						</ol>
					</nav>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
</section>
<!-- =======================
Inner intro END -->

<!-- =======================
Main content START -->
<section class="position-relative pt-0">
	<div class="container">
		<div class="row g-4">
      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card card-overlay-bottom card-img-scale">
          <!-- Card featured -->
					<span class="card-featured" title="Featured post"><i class="fas fa-star"></i></span>
          <!-- Card Image -->
          <img src="assets/images/blog/3by4/01.jpg" alt="">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-warning"><i class="fas fa-circle me-2 small fw-bold"></i>Technology</a>
            </div>
            <div class="w-100 mt-auto">

              <!-- Card title -->
              <h4 class="text-white"><a href="post-single.html" class="btn-link text-reset stretched-link">Best Pinterest boards for learning about business</a></h4>
              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Bryan</a>
                  </div>
                </li>
                <li class="nav-item">Aug 18, 2021</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->
      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card card-overlay-bottom card-image-scale">
          <!-- Card Image -->
          <img src="assets/images/blog/3by4/02.jpg" alt="">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-success mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Business</a>
            </div>
            <div class="w-100 mt-auto">
              <!-- Card title -->
              <h4 class="text-white"><a href="post-single.html" class="btn-link text-reset stretched-link">5 intermediate guide to business</a></h4>
              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Joan</a>
                  </div>
                </li>
                <li class="nav-item">Jun 03, 2021</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->
      <!-- Card item START -->
      <div class="col-12 col-lg-4">
        <div class="card card-overlay-bottom bg-parallax h-400 h-lg-100" data-jarallax-video="https://youtu.be/WS-MfCjzztg" data-speed="1.2" style="z-index: 0;">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
              <div>
                <!-- Card category -->
                <a href="#" class="badge bg-dark fs-6 mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Travel</a>
              </div>
            <div class="w-100 mt-auto">
              <!-- Card title -->
              <h4 class="text-white"><a href="post-single.html" class="btn-link text-reset stretched-link">5 investment doubts you should clarify</a></h4>
              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Dennis</a>
                  </div>
                </li>
                <li class="nav-item">Jan 28, 2021</li>
              </ul>
            </div>
          </div>
        <div style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; z-index: -100;" id="jarallax-container-0"><div style="background-position: 50% 50%; background-size: cover; background-repeat: no-repeat; background-image: url(&quot;https://img.youtube.com/vi/WS-MfCjzztgindex-6.html0.jpg&quot;); position: absolute; top: 0px; left: 0px; width: 373.983px; height: 594.68px; overflow: hidden; pointer-events: none; transform-style: preserve-3d; backface-visibility: hidden; will-change: transform, opacity; margin-top: 72.16px; transform: translate3d(0px, -247.88px, 0px); display: none;"></div><iframe id="VideoWorker-0" allowfullscreen="1" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" title="YouTube video player" src="https://www.youtube.com/embed/WS-MfCjzztg?autohide=1&amp;rel=0&amp;autoplay=0&amp;playsinline=1&amp;iv_load_policy=3&amp;modestbranding=1&amp;controls=0&amp;showinfo=0&amp;disablekb=1&amp;enablejsapi=1&amp;widgetid=1" style="position: absolute; inset: 0px; width: 768.498px; height: 832.28px; max-width: none; max-height: none; pointer-events: none; transform-style: preserve-3d; backface-visibility: hidden; will-change: transform, opacity; margin: -249.64px 0px 0px -197.257px; z-index: -1; transform: translate3d(0px, -72.2733px, 0px);" width="640" height="360" frameborder="0"></iframe></div></div>
      </div>
      <!-- Card item END -->
      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card card-overlay-bottom card-image-scale">
          <!-- Card Image -->
          <img src="assets/images/blog/3by4/04.jpg" alt="">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-primary mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Business</a>
            </div>
            <div class="w-100 mt-auto">
              <!-- Card title -->
              <h4 class="text-white"><a href="post-single.html" class="btn-link text-reset stretched-link">7 common mistakes everyone makes while traveling</a></h4>
              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Bryan</a>
                  </div>
                </li>
                <li class="nav-item">May 26, 2021</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->
      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card bg-primary h-400 h-sm-100">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-dark fs-6 mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Travel</a>
              <!-- Card title -->
              <blockquote class="blockquote blockquote-icon-bg mt-3">
								<p class="mb-0 text-white">Your time is limited, so don’t waste it living someone else’s life. Don’t be trapped by dogma – which is living with the results of other people’s thinking</p>
								<footer class="blockquote-footer text-white">Steve Jobs @ <cite title="Source Title">Apple Inc</cite></footer>
							</blockquote>
            </div>
            <div class="w-100 mt-auto">

              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="d-flex align-items-center position-relative">
                    <div class="avatar avatar-xs">
                      <img class="avatar-img rounded-circle" src="assets/images/avatar/01.jpg" alt="avatar">
                    </div>
                    <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Samuel</a></span>
                  </div>
                </li>
                <li class="nav-item">May 26, 2021</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->
      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card card-overlay-bottom card-image-scale">
          <!-- Card Image -->
          <img src="assets/images/blog/3by4/05.jpg" alt="">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-info mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Covid-19</a>
            </div>
            <div class="w-100 mt-auto">
              <!-- Card title -->
              <h4 class="text-white"><a href="post-single.html" class="btn-link text-reset stretched-link">Bad habits that people in the industry need to quit</a></h4>
              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Billy</a>
                  </div>
                </li>
                <li class="nav-item">Feb 22, 2021</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->
      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card bg-light border h-500 h-sm-100">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-warning mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Covid-19</a>
              <!-- Card title -->
              <h3><a href="#" class="btn-link text-reset">The pros and cons of business agency</a></h3>
              <p class="card-text d-none d-md-block">Interested has all Devonshire difficulty gay assistance joy.</p>
              <div class="mt-3 rounded overflow-hidden">
								<div class="ratio ratio-16x9">
									<iframe src="https://www.youtube.com/embed/tXHviS-4ygo" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="560" height="315"></iframe>
								</div>
							</div>
            </div>
            <div class="w-100 mt-auto">
              <!-- Card info -->
              <ul class="nav nav-divider align-items-center small">
                <li class="nav-item position-relative">
                  <div class="d-flex align-items-center position-relative">
                    <div class="avatar avatar-xs">
                      <img class="avatar-img rounded-circle" src="assets/images/avatar/05.jpg" alt="avatar">
                    </div>
                    <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Joan</a></span>
                  </div>
                </li>
                <li class="nav-item">Aug 28, 2022</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->
      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card card-overlay-bottom card-image-scale">
          <!-- Card Image -->
          <img src="assets/images/blog/3by4/06.jpg" alt="">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-primary mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Business</a>
            </div>
            <div class="w-100 mt-auto">
              <!-- Card title -->
              <h4 class="text-white"><a href="post-single.html" class="btn-link text-reset stretched-link">7 common mistakes everyone makes while traveling</a></h4>
              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Bryan</a>
                  </div>
                </li>
                <li class="nav-item">May 26, 2021</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->

      <!-- Card item START -->
      <div class="col-sm-6 col-lg-4">
        <div class="card card-overlay-bottom card-image-scale">
          <!-- Card Image -->
          <img src="assets/images/blog/3by4/07.jpg" alt="">
          <!-- Card Image overlay -->
          <div class="card-img-overlay d-flex flex-column p-3 p-md-4">
            <div>
              <!-- Card category -->
              <a href="#" class="badge bg-info mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Covid-19</a>
            </div>
            <div class="w-100 mt-auto">
              <!-- Card title -->
              <h4 class="text-white"><a href="post-single.html" class="btn-link text-reset stretched-link">Ten questions you should answer truthfully.</a></h4>
              <!-- Card info -->
              <ul class="nav nav-divider text-white-force align-items-center small">
                <li class="nav-item position-relative">
                  <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Billy</a>
                  </div>
                </li>
                <li class="nav-item">Feb 22, 2021</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Card item END -->
    </div> <!-- Row end -->

		<!-- Pagination START -->
    <nav class="my-5" aria-label="navigation">
      <ul class="pagination pagination-bordered d-flex justify-content-between">
        <li>
					<ul class="list-unstyled">
						<li class="page-item">
							<a class="page-link" href="#">First</a>
						</li>
					</ul>
				</li>
        <li>
					<ul class="list-unstyled">
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item active"><a class="page-link" href="#">2</a></li>
						<li class="page-item disabled"><a class="page-link" href="#">..</a></li>
						<li class="page-item"><a class="page-link" href="#">22</a></li>
						<li class="page-item"><a class="page-link" href="#">23</a></li>
					</ul>
				</li>
        <li>
					<ul class="list-unstyled">
						<li class="page-item">
							<a class="page-link" href="#">Last</a>
						</li>
					</ul>
				</li>
      </ul>
    </nav>
		<!-- Pagination END -->
	</div>
</section>
<!-- =======================
Main content END -->
@endsection

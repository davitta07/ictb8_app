@extends('layout.front')
@section('main_content')
<!-- =======================
Main hero START -->
@include('frontend.includes.main_hero')
<!-- =======================
Main hero END -->

<!-- =======================
Highlights START -->
@include('frontend.includes.hightlight')
<!-- =======================
Highlights END -->

<!-- =======================
Adv START -->
{{-- @include('frontend.includes.adv') --}}
<!-- =======================
Adv END -->

<!-- =======================
Small post START -->
@include('frontend.includes.smallpost')
<!-- =======================
Small post END -->

<!-- =======================
Tab post START -->
{{-- @include('frontend.includes.tappost') --}}
<!-- =======================
Tab post END -->
@endsection

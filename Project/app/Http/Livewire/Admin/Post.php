<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Post extends Component
{
    public function render()
    {
        return view('livewire.admin.post');
    }
}

<?php

use App\Http\Livewire\Admin\Post;

use App\Http\Livewire\Admin\Category;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Author\Dashboard;
use App\Http\Livewire\CategoryComponent;
use App\Http\Livewire\Author\Post as AuthorPost;
use App\Http\Livewire\Admin\Dashboard as AdminDashboard;
use App\Http\Livewire\Author\Category as AuthorCategory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('frontend.Home');
})->name('home');

Route::get('/sport', function () {
    return view('frontend.sport');
})->name('sport');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//admin
Route::middleware(['auth:sanctum', 'verified','authadmin'])->group(function(){
   Route::get('/admin/dashboard',AdminDashboard::class)->name('admin.dashboard');
   Route::get('/admin/categories',Category::class)->name('admin.categories');
   //Route::get('/admin/post',Post::class)->name('admin.post');
});
//author
Route::middleware(['auth:sanctum', 'verified','authauthor'])->group(function(){
   Route::get('/author/dashboard',Dashboard::class)->name('author.dashboard');
   Route::get('/author/categories',AuthorCategory::class)->name('author.categories');
  // Route::get('/author/categories',AuthorPost::class)->name('author.post');
});
// Category
// route::get('/',HomeComponent::class)->name('home');
route::get('/category',CategoryComponent::class)->name('front.categories');
route::get('/health',CategoryComponent::class)->name('front.categories');

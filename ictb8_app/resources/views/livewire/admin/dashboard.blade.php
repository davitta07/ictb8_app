<div>
		<!--start page wrapper -->
		<div class="page-wrapper">
			<div class="page-content">
				<div class="row row-cols-1 row-cols-lg-4">
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-cosmic">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Total Orders</p>
										<h5 class="mb-0 text-white">867</h5>
									</div>
									<div class="ms-auto text-white"><i class='bx bx-cart font-30'></i>
									</div>
								</div>
								<div class="progress bg-white-2 radius-10 mt-4" style="height:4.5px;">
									<div class="progress-bar bg-white" role="progressbar" style="width: 46%"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-burning">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Total Income</p>
										<h5 class="mb-0 text-white">$52,945</h5>
									</div>
									<div class="ms-auto text-white"><i class='bx bx-wallet font-30'></i>
									</div>
								</div>
								<div class="progress bg-white-2 radius-10 mt-4" style="height:4.5px;">
									<div class="progress-bar bg-white" role="progressbar" style="width: 72%"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-Ohhappiness">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Total Users</p>
										<h5 class="mb-0 text-white">24.5K</h5>
									</div>
									<div class="ms-auto text-white"><i class='bx bx-bulb font-30'></i>
									</div>
								</div>
								<div class="progress bg-white-2 radius-10 mt-4" style="height:4.5px;">
									<div class="progress-bar bg-white" role="progressbar" style="width: 68%"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-moonlit">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Comments</p>
										<h5 class="mb-0 text-white">869</h5>
									</div>
									<div class="ms-auto text-white"><i class='bx bx-chat font-30'></i>
									</div>
								</div>
								<div class="progress  bg-white-2 radius-10 mt-4" style="height:4.5px;">
									<div class="progress-bar bg-white" role="progressbar" style="width: 66%"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end row-->
				<div class="row">
					<div class="col">
						<div class="card radius-10 mb-0">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<h5 class="mb-1">User List</h5>
									</div>
									<div class="ms-auto">
										<a href="javscript:;" class="btn btn-primary btn-sm radius-30">Add New</a>
									</div>
								</div>

                               <div class="table-responsive mt-3">
								   <table class="table align-middle mb-0">
									   <thead class="table-light">
										   <tr>
											   <th>Photo</th>
											   <th>Name</th>
                                               <th>Mobile</th>
											   <th>Email</th>
                                               <th>Address</th>
											   <th>Status</th>
                                                <th>Action</th>

										   </tr>
									   </thead>
									   <tbody>
                                           @foreach ($users as $user)


										   <tr>
											   <td>
                                                <div class="product-img bg-transparent border">
												        <img src="{{URL('assets2/images/icons/smartphone.png')}}" class="p-1" alt="">
											    </div>
                                                </td>
											   <td>{{$user->name}}</td>
                                                <td>{{$user->mobile}}</td>
											   <td>{{$user->email}}</td>
                                                <td>{{$user->address}}</td>
											   <td class=""><span class="badge bg-light-success text-success w-100">{{$user->user_type}}</span></td>
											   <td>
                                                @if (Auth::user()->user_type==="author")

                                                @else
												<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class="bx bxs-trash"></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class="bx bxs-edit"></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class="fas fa-eye"></i></i></a>
                                                @endif
												</div>
											   </td>
										   </tr>
                                            @endforeach
									   </tbody>

								   </table>
							   </div>

							</div>
						</div>
					</div>
				</div>
                <div class="my-4">
                    {{ $users->links() }}
                </div>



		<footer class="page-footer">
			<p class="mb-0">Copyright © 2021. All right reserved.</p>
		</footer>
	</div>
	<!--end wrapper-->


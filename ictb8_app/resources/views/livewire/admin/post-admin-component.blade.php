<div>
  <div>
		<!--start page wrapper -->
		<div class="page-wrapper">
			<div class="page-content">

				<div class="row">
					<div class="col">
						<div class="card radius-10 mb-0">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<h5 class="mb-1">All Article</h5>
									</div>
									<div class="ms-auto">
										<a href="javscript:;" class="btn btn-primary btn-sm radius-30">Add New</a>
									</div>
								</div>

                               <div class="table-responsive mt-3">
								   <table class="table align-middle mb-0">
									   <thead class="table-light">
										   <tr>
											   <th>Photo</th>
											   <th>Title</th>
                                               <th>Category</th>
											   <th>Posted By</th>
                                               <th>Post Date</th>
											   <th>Status</th>
                                                <th>Action</th>

										   </tr>
									   </thead>
									   <tbody>
                                           @foreach ($posts as $post)


										   <tr>
											   <td>
                                                <div class="product-img bg-transparent border">
												        <img src="{{URL('assets/images/blog/4by3')}}/{{$post->image}}" class="p-1" alt="">
											    </div>
                                                </td>
											   <td>{!!strlen($post->title)>40?substr($post->title,0,20)."...":$post->title!!}</td>
                                                <td>{{$post->Category->name??''}}</td>
											   {{-- <td>{{$post->user_id}}</td> --}}
                                               <td>{{$post->user->name??''}}</td>

                                              <td>{{ KhmerDateTime\KhmerDateTime::parse($post->created_at)->fromNow()}}</td>
                                                @if ($post->status==1)
                                               <td>
                                                   <a href="javaScript:;" class="btn btn-sm btn-success radius-30">សកម្ម</a>
										        </td>
                                                @else
                                                <td>
                                                    <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">អសកម្ម</a>
										        </td>

                                                @endif
                                                
                                                    @if (Auth::user()->user_type=='author')

                                                    @endif
											   {{-- <td class=""><span class="badge bg-light-success text-success w-100">{{$user->user_type}}</span></td> --}}
											   <td>
												<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class="bx bxs-trash"></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class="bx bxs-edit"></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class="fas fa-eye"></i></i></a>

												</div>
											   </td>
										   </tr>
                                            @endforeach
									   </tbody>

								   </table>
							   </div>

							</div>
						</div>
					</div>
				</div>
                <div class="my-4">
                    {{ $posts->links() }}
                </div>



		<footer class="page-footer">
			<p class="mb-0">Copyright © 2021. All right reserved.</p>
		</footer>
	</div>
	<!--end wrapper-->

</div>

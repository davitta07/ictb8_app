<header>
	<!-- Navbar top -->
    <div class="navbar-top d-none d-lg-block small">
		<div class="container">
			<div class="d-md-flex justify-content-between align-items-center my-2">
				<!-- Top bar left -->
				<ul class="nav">
					<li class="nav-item">
						<a class="nav-link ps-0" href="about-us.html">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Forum</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="https://themes.getbootstrap.com/store/webestica/">Buy now!</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('login')}}">Login / Join</a>
					</li>
				</ul>
				<!-- Top bar right -->
				<div class="d-flex align-items-center">
					<ul class="nav">
						<li class="nav-item">
							<a class="nav-link px-2 fs-5" href="#"><i class="fab fa-facebook-square"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link px-2 fs-5" href="#"><i class="fab fa-twitter-square"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link px-2 fs-5" href="#"><i class="fab fa-linkedin"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link px-2 fs-5" href="#"><i class="fab fa-youtube-square"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link ps-2 pe-0 fs-5" href="#"><i class="fab fa-vimeo"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<!-- Divider -->
			<div class="border-bottom border-2 border-primary opacity-1"></div>
		</div>
	</div>
	{{-- <div class="navbar-top d-none d-lg-block">
		<div class="container">
			<div class="row d-flex align-items-center my-2">
				<!-- Top bar left -->
				<div class="col-sm-8 d-sm-flex align-items-center">
					<!-- Title -->
					<div class="me-3">
						<span class="badge bg-primary p-2 px-3 text-white">Trending:</span>
					</div>
					<!-- Slider -->
					<div class="tiny-slider arrow-end arrow-xs arrow-bordered arrow-round arrow-md-none">
						<div class="tiny-slider-inner"
							data-autoplay="true"
							data-hoverpause="true"
							data-gutter="0"
							data-arrow="true"
							data-dots="false"
							data-items="1">
							<!-- Slider items -->
							<div> <a href="#" class="text-reset btn-link">The most common business debate isn't as black and white as you might think</a></div>
							<div> <a href="#" class="text-reset btn-link">How the 10 worst business fails of all time could have been prevented </a></div>
							<div> <a href="#" class="text-reset btn-link">The most common business debate isn't as black and white as you might think </a></div>
						</div>
					</div>
				</div>

				<!-- Top bar right -->
				<div class="col-sm-4">
					<ul class="list-inline mb-0 text-center text-sm-end">
						<li class="list-inline-item">
							<span>Wed, March 31, 2021</span>
						</li>
						<li class="list-inline-item">
							<i class="bi bi-cloud-hail text-info"></i>
							<span>13 °C NY, USA</span>
						</li>
					</ul>
				</div>
			</div>
			<!-- Divider -->
			<div class="border-bottom border-2 border-primary opacity-1"></div>
		</div>
	</div> --}}
	<!-- Navbar logo section START -->
	<div>
		<div class="container">
			<div class="d-sm-flex justify-content-sm-between align-items-sm-center my-2">
				<!-- Logo START -->
				<a class="navbar-brand d-block" href="{{route('home')}}">
					<img class="navbar-brand-item light-mode-item" src="{{URL::to('assets/images/logo.svg')}}" alt="logo" style="height: 46px;">
					<img class="navbar-brand-item dark-mode-item" src="{{URL::to('assets/images/logo-light.svg')}}" alt="logo" style="height: 46px;">
				</a>
				<!-- Logo END -->
				<!-- Adv -->
				<div>
					<a href="#" class="card-img-flash d-block">
						<img src="{{URL::to('assets/images/adv-2.png')}}" alt="adv">
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Navbar logo section END -->

	<!-- Navbar START -->
	<div class="navbar-dark navbar-sticky header-static">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<div class="w-100 bg-dark d-flex">

					<!-- Responsive navbar toggler -->
					<button class="navbar-toggler me-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="text-muted h6 ps-3">Menu</span>
						<span class="navbar-toggler-icon"></span>
					</button>

					<!-- Main navbar START -->
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav navbar-nav-scroll navbar-lh-sm">
                                {{-- nav 01 --}}
                            <li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle active" href="" id="homeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ទំព័រដើម</a>
								<ul class="dropdown-menu" aria-labelledby="homeMenu">
									<li> <a class="dropdown-item {{request()->is('category')? 'active':''}}" href="{{route('front.categories')}}">សុខភាព</a></li>
									<li> <a class="dropdown-item {{request()->is('health')? 'active':''}}" href="{{route('front.categories')}}">កម្សាន្ត</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">កីឡា</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">ទេសចរណ៍</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">ជំនួញ</a></li>
								</ul>
							</li>
                                {{-- nav 02 --}}
                            <li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="knowledgeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ចំណេះដឹង</a>
								<ul class="dropdown-menu" aria-labelledby="knowledgeMenu">
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">ចំណេះដឹងទូទៅ</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">ប្រវត្តិសាស្ត្រ</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">គណិតសាស្ត្រ</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">អក្សរសាស្ត្រខ្មែរ</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">អង់គ្លេស</a></li>
								</ul>
							</li>
                                {{-- nav 03 --}}
                            <li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="sethakechMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">សេដ្ឋកិច្ច</a>
								<ul class="dropdown-menu" aria-labelledby="sethakechMenu">
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">សំណង់</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">សណ្ឋាគារនិងទេសចរណ៍</a></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">កសិកម្ម</a></li>
									<li class="dropdown-divider"></li>
									<li> <a class="dropdown-item" href="{{route('front.categories')}}">ការវិនិយោគនិងស្តុក</a></li>
								</ul>
							</li>
	                        <!-- Nav item 4 Mega menu -->
							<li class="nav-item dropdown dropdown-fullwidth">
								<a class="nav-link dropdown-toggle" href="#" id="megaMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ជីវិត និង សង្គម</a>
								<div class="dropdown-menu" aria-labelledby="megaMenu">
									<div class="container">
										<div class="row g-4 p-3 flex-fill">
											<!-- Card item START -->
											<div class="col-sm-6 col-lg-3">
												<div class="card bg-transparent">
													<!-- Card img -->
													<img class="card-img rounded" src="{{URL::to('assets/images/blog/16by9/small/01.jpg')}}" alt="Card image">
													<div class="card-body px-0 pt-3">
														<h6 class="card-title mb-0"><a href="#" class="btn-link text-reset fw-bold">7 common mistakes everyone makes while traveling</a></h6>
														<!-- Card info -->
														<ul class="nav nav-divider align-items-center text-uppercase small mt-2">
															<li class="nav-item">
																<a href="#" class="text-reset btn-link">Joan Wallace</a>
															</li>
															<li class="nav-item">Feb 18, 2021</li>
														</ul>
													</div>
												</div>
											</div>
											<!-- Card item END -->
											<!-- Card item START -->
											<div class="col-sm-6 col-lg-3">
												<div class="card bg-transparent">
													<!-- Card img -->
													<img class="card-img rounded" src="{{URL::to('assets/images/blog/16by9/small/02.jpg')}}" alt="Card image">
													<div class="card-body px-0 pt-3">
														<h6 class="card-title mb-0"><a href="#" class="btn-link text-reset fw-bold">12 worst types of business accounts you follow on Twitter</a></h6>
														<!-- Card info -->
														<ul class="nav nav-divider align-items-center text-uppercase small mt-2">
															<li class="nav-item">
																<a href="#" class="text-reset btn-link">Lori Stevens</a>
															</li>
															<li class="nav-item">Jun 03, 2021</li>
														</ul>
													</div>
												</div>
											</div>
											<!-- Card item END -->
											<!-- Card item START -->
											<div class="col-sm-6 col-lg-3">
												<div class="card bg-transparent">
													<!-- Card img -->
													<img class="card-img rounded" src="{{URL::to('assets/images/blog/16by9/small/03.jpg')}}" alt="Card image">
													<div class="card-body px-0 pt-3">
														<h6 class="card-title mb-0"><a href="#" class="btn-link text-reset fw-bold">Skills that you can learn from business</a></h6>
														<!-- Card info -->
														<ul class="nav nav-divider align-items-center text-uppercase small mt-2">
															<li class="nav-item">
																<a href="#" class="text-reset btn-link">Judy Nguyen</a>
															</li>
															<li class="nav-item">Sep 07, 2021</li>
														</ul>
													</div>
												</div>
											</div>
											<!-- Card item END -->
											<!-- Card item START -->
											<div class="col-sm-6 col-lg-3">
												<div class="bg-primary-soft p-4 text-center h-100 w-100 rounded">
													<span>The Blogzine</span>
													<h3>Premium Membership</h3>
													<p>Become a Member Today!</p>
													<a href="#" class="btn btn-warning">View pricing plans</a>
												</div>
											</div>
											<!-- Card item END -->
										</div> <!-- Row END -->
										<!-- Trending tags -->
										<div class="row px-3">
											<div class="col-12">
												<ul class="list-inline mt-3">
													<li class="list-inline-item">Trending tags:</li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-primary-soft">Travel</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-warning-soft">Business</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-success-soft">Tech</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-danger-soft">Gadgets</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-info-soft">Lifestyle</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-primary-soft">Vaccine</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-success-soft">Sports</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-danger-soft">Covid-19</a></li>
													<li class="list-inline-item"><a href="#" class="btn btn-sm btn-info-soft">Politics</a></li>
												</ul>
											</div>
										</div> <!-- Row END -->
									</div>
								</div>
							</li>
                            {{-- nav 5 --}}
                            <li class="nav-item">	<a class="nav-link" href="#">កូវីដ-១៩</a></li>
						</ul>
					</div>
					<!-- Main navbar END -->

					<!-- Nav right START -->
					<div class="nav flex-nowrap align-items-center me-2">
						<!-- Dark mode switch -->
						<div class="nav-item">
							<div class="modeswitch" id="darkModeSwitch">
								<div class="switch"></div>
							</div>
						</div>
						<!-- Nav bookmark -->
						<div class="nav-item">
							<a class="nav-link" href="#">
								<i class="bi bi-bookmark-heart fs-4"></i>
							</a>
						</div>
						<!-- Nav user -->
                        @if (Route::has('login'))
                            @auth
                            <li class="nav-item dropdown">
								<a class="nav-link" href="" id="knowledgeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="avatar avatar-xs">
										<img class="avatar-img rounded-circle" src="{{URL::to('assets/images/avatar/03.jpg')}}" alt="avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{Auth::user()->name}}">
									</div>
                                </a>
                                @if (Auth::user()->user_type==="admin")


								<ul class="dropdown-menu" aria-labelledby="knowledgeMenu">
									<li> <a class="dropdown-item" href="{{route('admin.dashboard')}}">Dashboard Admin</a></li>
									<li> <a class="dropdown-item" href="{{route('admin.categories')}}">Categories</a></li>
									<li> <a class="dropdown-item" href="{{route('admin.post')}}">Posts</a></li>
									<li> <a class="dropdown-item" href="{{route('logout')}}"
                                     onclick="event.preventDefault();document.getElementById('logout-form').submit();">logout</a>
                                    </li>
								</ul>

                                @elseif (Auth::user()->user_type==="author")
                                <ul class="dropdown-menu" aria-labelledby="knowledgeMenu">
									<li> <a class="dropdown-item" href="{{route('author.dashboard')}}">Dashboard Author</a></li>
									<li> <a class="dropdown-item" href="{{route('author.categories')}}">Categories</a></li>
									<li> <a class="dropdown-item" href="{{route('author.post')}}">Posts</a></li>
									<li> <a class="dropdown-item" href="{{route('logout')}}"
                                     onclick="event.preventDefault();document.getElementById('logout-form').submit();">logout</a>
                                    </li>
								</ul>
                                @endif
                                <form action="{{route('logout')}}" id="logout-form" method="post" style="display: none">
                                    @csrf
                                </form>
                             </li>
						{{-- <div class="nav-item">
							<a class="nav-link" href="#">
								<i class="bi bi-person-square fs-4"></i>
							</a>
						</div> --}}

                        @endauth
                        @endif
						<!-- Nav Search -->
						<div class="nav-item dropdown nav-search dropdown-toggle-icon-none">
							<a class="nav-link text-uppercase dropdown-toggle" role="button" href="#" id="navSearch" data-bs-toggle="dropdown" aria-expanded="false">
								<i class="bi bi-search fs-4"> </i>
							</a>
							<div class="dropdown-menu dropdown-menu-end shadow rounded p-2" aria-labelledby="navSearch">
								<form class="input-group">
									<input class="form-control border-success" type="search" placeholder="Search" aria-label="Search">
									<button class="btn btn-success m-0" type="submit">Search</button>
								</form>
							</div>
						</div>
						<!-- Offcanvas menu toggler -->
						<div class="nav-item">
							<a class="nav-link pe-0" data-bs-toggle="offcanvas" href="#offcanvasMenu" role="button" aria-controls="offcanvasMenu">
								<i class="bi bi-text-right rtl-flip fs-2" data-bs-target="#offcanvasMenu"> </i>
							</a>
						</div>
					</div>
					<!-- Nav right END -->
				</div>
			</div>
		</nav>
	</div>
	<!-- Navbar END -->
</header>

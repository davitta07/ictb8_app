	<div class="wrapper">
		<!--sidebar wrapper -->
		<div class="sidebar-wrapper" data-simplebar="true">
			<div class="sidebar-header">
				<div>
                <a class="navbar-brand d-block" href="{{route('home')}}">
					<img class="navbar-brand-item light-mode-item" src="{{URL::to('assets/images/logo.svg')}}" alt="logo" style="height: 25px;">
					<img class="navbar-brand-item dark-mode-item" src="{{URL::to('assets/images/logo-light.svg')}}" alt="logo" style="height: 25px;">
				</a>
				</div>
			</div>
			<!--navigation-->
			<ul class="metismenu" id="menu">
				<li>
					<a href="{{route('admin.dashboard')}}" class="">
						<div class="parent-icon"><i class='bx bx-home'></i>
						</div>
						<div class="menu-title">Dashboard</div>
					</a>
				</li>
				<li>
					<a href="javascript:;" class="has-arrow">
						<div class="parent-icon"><i class='bx bx-home'></i>
						</div>
						<div class="menu-title">Article</div>
					</a>
					<ul>
						<li> <a href="{{route('admin.post')}}"><i class="bx bx-right-arrow-alt"></i>Post</a>
						</li>
						<li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Category</a>
						</li>
                        <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Ads</a>
                        </li>
						<li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Slider</a>
						</li>
                        <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Author</a>
						</li>
					</ul>
				</li>

			<!--end navigation-->
		</div>


<!doctype html>
<html lang="en" class="color-sidebar sidebarcolor3 color-header headercolor1">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
	<link rel="shortcut icon" href="{{URL::to('assets/images/favicon.ico')}}">
	<!--plugins-->
	<link href="{{URL::to('assets2/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
	<link href="{{URL::to('assets2/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" />
	<link href="{{URL::to('assets2/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
	<!-- loader-->
	<link href="{{URL::to('assets2/css/pace.min.css')}}" rel="stylesheet" />
	<script src="{{URL::to('assets2/js/pace.min.js')}}"></script>
	<!-- Bootstrap CSS -->
	<link href="{{URL::to('assets2/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<link href="{{URL::to('assets2/css/app.css')}}" rel="stylesheet">
	<link href="{{URL::to('assets2/css/icons.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	<!-- Theme Style CSS -->
	<link rel="stylesheet" href="{{URL::to('assets/css/dark-theme.css')}}" />
	<link rel="stylesheet" href="{{URL::to('assets/css/semi-dark.css')}}" />
	<link rel="stylesheet" href="{{URL::to('assets/css/header-colors.css')}}" />
	<title>Blogzine–Admin Dashboard</title>
</head>
@livewireStyles
<body>

    	<!--wrapper-->
         @include('Backend.partials.wrapper')
        <!--end sidebar wrapper -->

		<!--start header -->
        @include('Backend.header')
        <!--end header -->
        <!--main-->
        {{$slot}}
        <!--endmain-->

    <!-- Bootstrap JS -->
	<script src="{{URL::to('assets2/js/bootstrap.bundle.min.js')}}"></script>
	<!--plugins-->
	<script src="{{URL::to('assets2/js/jquery.min.js')}}"></script>
	<script src="{{URL::to('assets2/plugins/simplebar/js/simplebar.min.js')}}"></script>
	<script src="{{URL::to('assets2/plugins/metismenu/js/metisMenu.min.js')}}"></script>
	<script src="{{URL::to('assets2/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
	<script src="{{URL::to('assets2/plugins/apexcharts-bundle/js/apexcharts.min.js')}}"></script>
	<script src="{{URL::to('assets2/js/index3.js')}}"></script>
	<script>
		new PerfectScrollbar('.best-selling-products');
		new PerfectScrollbar('.recent-reviews');
		new PerfectScrollbar('.support-list');
	</script>
	<!--app JS-->
	<script src="{{URL::to('assets2/js/app.js')}}"></script>
    @livewireScripts
</body>

</html>

<?php

namespace Database\Factories;

use App\Models\Categories;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model=Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'images'=>$this->faker->buildingNumber(),
        'user_id'=>User::factory(),
        'category_id'=>Categories::factory(),
        'title'=>$this->faker->text(),
        'slug'=>$this->faker->slug(),
        'description'=>$this->faker->paragraph(),
        'details'=>$this->faker->paragraph(),
        'tags'=>$this->faker->text(),
        'source_title'=>$this->faker->text(),
        'source_link'=>$this->faker->text()
        ];
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->unique();

            $table->bigInteger('category_id')->unsigned()->nullable();

            $table->foreign('category_id')->references('id')->on('categories');

           $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('CASCADE');

            $table->string('slug');

            $table->longText('description')->nullable();

            $table->longText('details')->nullable();

            $table->string('tags')->nullable();

            $table->string('image')->nullable();

            $table->string('images')->nullable();

            $table->tinyInteger('status')->default(false);

            $table->string('source_title')->nullable();

            $table->string('source_link')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

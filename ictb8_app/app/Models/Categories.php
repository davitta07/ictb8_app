<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='categories';
    protected $fillable = [
        'name',
        'slug',
        'image',
        'icon',
        'color',
        'status',
        'for_menu'
    ];
    public function post()
    {
        return $this->BelongsTo(Post::class);
    }
}

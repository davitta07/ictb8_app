<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table='posts';
    protected $fillable=[
        'title',
        'category_id',
        'slug',
        'user_id',
        'description',
        'details',
        'tags',
        'image',
        'images',
        'status',
        'source_title',
        'source_link',
    ];
    public function Category()
    {
        return $this->BelongsTo(Categories::class);
    }
     public function user()
    {
        return $this->belongsTo(User::class);
    }
}



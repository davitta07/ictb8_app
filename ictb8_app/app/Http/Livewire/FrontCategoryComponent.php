<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FrontCategoryComponent extends Component
{
    public function render()
    {
        return view('livewire.front-category-component')->layout('layouts.base');
    }
}

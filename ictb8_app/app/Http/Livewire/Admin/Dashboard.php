<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\User;

class Dashboard extends Component
{

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $users=User::paginate(6);
        return view('livewire.admin.dashboard',["users"=>$users]
        )->layout('Backend.layouts.base');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;


class PostAdminComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $posts=Post::paginate(6);
        return view('livewire.admin.post-admin-component'
        ,['posts'=>$posts]
        )->layout('Backend.layouts.base');
    }
}

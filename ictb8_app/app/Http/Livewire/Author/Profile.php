<?php

namespace App\Http\Livewire\Author;

use Livewire\Component;

class Profile extends Component
{
    public function render()
    {
        return view('livewire.author.profile')->layout('layouts.base');
    }
}

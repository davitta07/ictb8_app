<?php

namespace App\Http\Livewire\Author;

use Livewire\Component;

class Post extends Component
{
    public function render()
    {
        return view('livewire.author.post')->layout('layouts.base');
    }
}
